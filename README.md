# Comparea

A simple tool for visually comparing between two maps. When zooming, maps synchronise to the same scale and relative areas can be seen (approximately). 

Built with:
- Vue.js
- The ArcGIS Javascript API v4 and [esri-loader](https://github.com/Esri/esri-loader)


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
